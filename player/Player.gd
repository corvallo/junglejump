extends KinematicBody2D

const StateMachineFactory = preload('res://player/states/state_machine_factory.gd')
const IdleState = preload("res://player/states/idle.gd")
const MoveState = preload("res://player/states/move.gd")
const HurtState = preload("res://player/states/hurt.gd")
const JumpState = preload("res://player/states/jump.gd")

onready var sfm = StateMachineFactory.new()

export (int) var run_speed
export (int) var jump_speed
export (int) var gravity

var states
var motion = Vector2()
var normal = Vector2(0,-1)

signal life_changed
signal dead

var life
var friction = false

func start(pos):
	position = pos
	

func _ready():	
	life = 3
	emit_signal('life_changed',life)
	states = sfm.create({
		'target':self,
		'current_state': 'idle',
		'states':[
			{"id":"idle","state":IdleState},
			{"id":"move","state":MoveState},
			{"id":"hurt","state":HurtState},
			{"id":"jump","state":JumpState}
			
		],
		"transitions":[
			{"state_id": "idle", "to_states": ["move","hurt","jump"]},
			{"state_id": "move", "to_states": ["idle","hurt","jump"]},
			{"state_id": "jump", "to_states": ["idle","hurt","move"]},
			{"state_id": "hurt", "to_states": ["idle"]}
		]
	})	
	states._ready()
	pass

func _process(delta): 	
	motion.y += 20 
	friction = false
	normal = Vector2(0,-1)	
	states._process(delta)
	motion = move_and_slide(motion,normal)
	for idx in range(get_slide_count()):
		var collision = get_slide_collision(idx)
		if collision.collider.name == "Dangers":
			states.transition("hurt")

func say(msg):
	print(msg)



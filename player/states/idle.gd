extends "state.gd"

func _ready():
	_on_enter_state()
	playAnimation('idle')	

func _process(delta):
	playAnimation('idle')
	target.motion.x = lerp(0,0,0.2)
	if Input.is_action_pressed("left") or Input.is_action_pressed("right") :
		state_machine.transition("move")	
	if Input.is_action_just_pressed("jump"):
		state_machine.transition("jump")

func _on_enter_state():
	target.say("Idle state")	

func _on_leave_state():
	target.say("finish idle state")
extends "state.gd"


var movement = Vector2()

func _process(delta):
	movement = Vector2()
	var SPRITE = target.get_node("AnimatedSprite")	
	if Input.is_action_pressed("right"):
		movement.x = movement.x + target.run_speed
		SPRITE.flip_h = false
		playAnimation('run')
	elif Input.is_action_pressed("left"):
		movement.x = movement.x - target.run_speed
		SPRITE.flip_h = true
		playAnimation('run')
	else:
		movement.x = lerp(movement.x,0,0.2)
		target.friction=true
		
	if Input.is_action_just_released("left") or Input.is_action_just_released("right"):		
		state_machine.transition("idle")
	if Input.is_action_just_pressed("jump"):
		state_machine.transition("jump")

	movement = target.move_and_slide(movement,target.normal)
	pass
		
func _on_enter_state():
	target.say("Move state")	

func _on_leave_state():
	target.say("finish move state")
extends "state.gd"

var movement = Vector2()
var double_jumping = false
var raycasting


func _process(delta):	
	movement = Vector2()	
	var SPRITE = target.get_node("AnimatedSprite")	
	if Input.is_action_just_pressed("jump") and not double_jumping:
		playAnimation('jump_up')
		double_jumping = true
		target.motion.y = target.jump_speed 		
		if target.friction == true:
			target.motion.x = lerp(target.motion.x,0,0.2)


	if !Input.is_action_pressed("jump"):
		if target.motion.y >=0:			
			playAnimation('jump_down')
		if raycasting.is_colliding():
			double_jumping = false
			state_machine.transition("idle")		
	if Input.is_action_pressed("right"):		
		target.motion.x = movement.x + target.run_speed
		SPRITE.flip_h = false
	if Input.is_action_pressed("left"):		
		target.motion.x = movement.x - target.run_speed
		SPRITE.flip_h = true

	pass
	
func jump():	
	
	if raycasting.is_colliding():
		double_jumping = false		
		playAnimation('jump_up')
		target.motion.y = target.jump_speed 		
		if target.friction == true:
			target.motion.x = lerp(target.motion.x,0,0.2)

func _on_PlayerSprite_animation_finished():
	if raycasting.is_colliding():
		state_machine.transition("idle")

func _on_enter_state():
	raycasting = target.get_node("RayCast2D")
	target.say("Jump state")
	jump()

func _on_leave_state():
	target.say("Finish jump state")
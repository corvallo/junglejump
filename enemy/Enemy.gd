extends KinematicBody2D

export (int) var speed
export (int) var gravity
onready var parent = get_parent()

var velocity = Vector2()
var tempPos = Vector2()
var startingPos = Vector2()
var direction
const MAX_SPEED = 50
const ACCELERATION = 30
const MAX_RANGE = 270
func _ready():
	randomize()
	$AnimationPlayer.play('walk')
	tempPos = position
	startingPos = position
	direction = randi()%2	
	$SpriteDead.hide()
	pass

func _process(delta):
	
	var current_position = position.x
	
	if direction:
		if current_position > (startingPos.x + MAX_RANGE):			
			$Sprite.flip_h = false
			direction=0
			velocity.x = max(-velocity.x -ACCELERATION,-MAX_SPEED)
		else: 
			$Sprite.flip_h = true
			velocity.x = min(velocity.x + ACCELERATION, MAX_SPEED)
	else :
		if current_position < (startingPos.x - MAX_RANGE):
			$Sprite.flip_h = true
			direction = 1
			velocity.x = min(-velocity.x + ACCELERATION, MAX_SPEED)
		else:
			velocity.x = max(velocity.x -ACCELERATION,-MAX_SPEED)
	
	if $RayCast2D.is_colliding():
		if not $SpriteDead.visible:
			$Sprite.hide()
			$SpriteDead.show()
			$AnimationDead.play("dead")		
			print($RayCast2D.get_collider().get_name())	

	velocity.y = 20
	velocity = move_and_slide(velocity,Vector2(0,-1))
	pass


func _on_AnimationDead_animation_finished(anim_name):
	queue_free()
	pass # replace with function body

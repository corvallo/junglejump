extends Area2D


signal pickup

var textures = {'cherry':'res://assets/spritess/cherry.png',
				'gem': 'res://assets/spriters/gem.png'}


func init(type,pos):
	$Sprite.texture =load(textures[type])
	position = pos


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass



func _on_Collectible_body_entered(body):
	emit_signal("pickup")
	queue_free()
	pass # replace with function body

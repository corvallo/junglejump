extends Node2D

onready var pickups = $Pickups

func _ready():
	pickups.hide()
	$Player.start($PlayerSpawn.position)
	$Player.show()
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
